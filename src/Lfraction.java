import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      this.numerator = a;
      if (b == 0) {
         throw new RuntimeException("Nimetaja ei saa olla 0.");
      }
      this.denominator = b;
   }

   public Lfraction reduce (Lfraction o) {
      long common = 0;
      
      long num = Math.abs(o.numerator);
      long den = Math.abs(o.denominator);
      
      if (num > den) common = gcd(num, den);
      else if (num < den) common = gcd(den, num);
      else common = num;
      
      long numerator = o.numerator / common;
      long denominator = o.denominator / common;
      return new Lfraction(numerator, denominator);
   }

   private long gcd(long d1, long d2)
   {
      long factor = d2;
      while (d2 != 0) {
         factor = d2;
         d2 = d1 % d2;
         d1 = factor;
      }
      return d1;
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.numerator + " / " + this.denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return (this.compareTo((Lfraction)m) == 0);
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return (int) (Double.doubleToLongBits (toDouble())>>31);
   }

   private long commonDenominator(long a, long b) {
      return a * b;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long cd = commonDenominator(this.denominator, m.denominator);
      long temp1 = this.numerator * m.denominator;
      long temp2 = m.numerator * this.denominator;
      long answer = temp1 + temp2;

      Lfraction obj = new Lfraction(answer, cd);
      return reduce(obj);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long korrutame1 = this.numerator * m.numerator;
      long korrutame2 = this.denominator * m.denominator;

      Lfraction korrutatudObjekt = new Lfraction(korrutame1, korrutame2);
      return reduce(korrutatudObjekt);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      long negNum = this.numerator < 0 ? -1 : 1;
      long negDen = this.denominator < 0 ? -1 : 1;
      long num = this.denominator*negNum*negDen;
      long den = this.numerator*negNum*negDen;
      if (den == 0) throw new RuntimeException("Nulliga ei saa jagada");
      Lfraction turnAround = new Lfraction(num,den);
      return turnAround;
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {

      long miinuseksMuutja = -1;
      Lfraction minusM2rk = new Lfraction(miinuseksMuutja * this.numerator, this.denominator);
      return minusM2rk;
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      long cd = commonDenominator(this.denominator, m.denominator);
      long temp1 = this.numerator * m.denominator;
      long temp2 = m.numerator * this.denominator;
      long answer = temp1 - temp2;

      Lfraction minusObject = new Lfraction(answer, cd);
      return reduce(minusObject);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      long korrutame1 = this.numerator * m.denominator;
      long korrutame2 = this.denominator * m.numerator;
      if (korrutame2 == 0) throw new RuntimeException("Nulliga ei saa jagada");
      Lfraction jagatudObjekt = new Lfraction(korrutame1, korrutame2);
      return reduce(jagatudObjekt);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction dif = this.minus(m);
      if (dif.numerator == 0) {
         return 0;
      } else if (dif.numerator < 0) {
         return -1;
      } else {
         return 1;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      Lfraction uus = new Lfraction(this.numerator, this.denominator);
      return uus;
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (long) Math.floor(Double.valueOf(this.numerator / this.denominator));
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return reduce(new Lfraction(this.numerator - this.denominator*integerPart(), this.denominator));
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      double uusDouble1 = (double) this.numerator;
      double uusDouble2 = (double) this.denominator;
      return uusDouble1 / uusDouble2;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long muutuja = Math.round(d * f);
      Lfraction objekt = new Lfraction((long) muutuja, d);
      return objekt;
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] osad = s.split("/");
      if (osad.length > 2) throw new RuntimeException("Kirjutis pole murd: " + s);
      try {
        long num = Long.parseLong(osad[0].trim());
      } catch (Excception e) {
        throw new RuntimeException("tundmatu lugeja: " + s);
      }
      try {
        long den = Long.parseLong(osad[1].trim());
      } catch (Exception e) {
        throw new RuntimeException("Tundmatu numetaja: " + s);
      }
      Lfraction valueOfObject = new Lfraction(Long.parseLong(osad[0].trim()), Long.parseLong(osad[1].trim()));
      return valueOfObject;
   }
}